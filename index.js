// console.log("Happy Thursday");

let number = prompt("Enter a number:");

for (let i = number; i > 0; i--) {
  // Check if current value is less than or equal to 50
  if (i <= 50) {
    console.log("The current value is at "+ i + ". Terminating the loop.")
    break;
  }

  if (i % 10 === 0) {
    console.log("Number " + i + " is being skipped");
    continue;
  }
  if (i % 5 === 0) {
    console.log(i);
  }
}


let word = "supercalifragilisticexpialidocious";
let myConsonants = "";

function loopString() {
  for (let i = 0; i < word.length; i++) {
    if (word[i] == "a" || word[i] == "e" || word[i] == "i" || word[i] == "o" || word[i] == "u") {
      continue;
    } else {
      myConsonants += word[i];
    }
  }
}

    loopString();
    console.log(word);
    console.log(myConsonants);